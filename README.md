# springer

### Answers for Task 1 are in the file - exploratorySession.docx

### Answers for task 3 and 4 are in line in the file - QA Assessment 2018.docx

### For task 2:

I have used webdriverIO along with Mocha and Chai to execute tests for Search functionality.

Please note that these tests were created and tested on the following environment:
	1. node - v8.9.4 or higher
	2. npm - 5.6.0
	3. firefox browser - 61.0.2 (64-bit)

### Navigate to the tests folder
```console
$ cd automation-tests/
```

### Install the relevant framework by executing the following command 
```console
$ npm install
```

### Execute the tests using
```console
$ npm test
```