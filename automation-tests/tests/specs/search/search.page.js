import HomePage from '../../pageobjects/pages/home.page.js'
import SearchResultsPage from '../../pageObjects/pages/search.results.page'
let expect = require('chai').expect

describe( 'When a member browses to the home page', function (){
    it ('Then the search bar should be visible', function(){
        HomePage.open('live','home');
        expect( HomePage.isHomePage()).to.equal(true);    
    });

    it ('And after searching for Biology, results should be displayed ' ,function(){
        HomePage.searchBar.setValue('Biology');
        HomePage.startSearch();
        expect(SearchResultsPage.areResultsDisplayed()).to.equal(true)
    });

    it ('And book result for Studies in Human Biology should be displayed',function(){
        const bookResult = '//*[@id="results-list"]/li[10]/div[2]/h2/a'        
        expect(browser.element(bookResult).getText()).to.equal('Studies in Human Biology')
        
    });

});
