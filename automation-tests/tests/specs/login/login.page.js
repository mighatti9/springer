import LoginPage from '../../pageobjects/pages/login.page.js'
import HomePage from '../../pageobjects/pages/home.page.js'

let expect = require('chai').expect

describe( 'When a member browses to the login page', function (){
    it ('Then should be able to login with valid credentials', function(){
        LoginPage.open('live','login');
        
        LoginPage.username.setValue('shekhar.karande@gmail.com');
        LoginPage.password.setValue('pearauto671');
        LoginPage.submit();
        expect( HomePage.isHomePage()).to.equal(true);       
    });

});
