import pageNames from '../constants/environments'


export function getBaseURL (environment, pageName){
    if(!environment) throw Error("Incorrect environment value")

    if(!pageNames[environment]) throw Error("Environment undefined")

    if(!pageNames[environment][pageName]) throw Error("Page undefined")

    return pageNames[environment][pageName]
}

export default {
    getBaseURL: getBaseURL
}