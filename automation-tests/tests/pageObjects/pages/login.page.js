import Page from './page';
import Environment from '../../services/environments'

//declaring selectors

const usernameSelector = '//*[@id="login-box-email"]';
const passwordSelector = '//*[@id="login-box-pw"]';
const loginSelector = '//*[@id="login-box"]/div/div[3]/button';
const flashMessageSelector = '//*[@id="login-box"]/div[1]';

class LoginPage extends Page {
    get username()  { return browser.element(usernameSelector); }
    get password()  { return browser.element(passwordSelector); }
    get login()     { return browser.element(loginSelector); }
    get flash()     { 
        browser.waitForVisible(flashMessageSelector);
        return browser.element(flashMessageSelector); 
    }

    open(environment, pageName) {
        let url = Environment.getBaseURL(environment,pageName);        
        super.open(url);
    }

    submit() {
        this.login.submitForm();
    }

    isLoginVisible(){
        return this.login.isExisting()
    }

    isNotLoggedIn(){
        return this.flash.isExisting()
    }

}

export default new LoginPage();