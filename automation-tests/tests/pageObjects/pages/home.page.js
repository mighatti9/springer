import Page from './page';
import Environment from '../../services/environments'


const searchBarSelector = '//*[@id="query"]';
const searchSubmitSelector = '//*[@id="search"]'

class HomePage extends Page{
    get searchBar(){
        browser.waitForVisible(searchBarSelector);
        return browser.element(searchBarSelector);
    }

    get search(){ return browser.element(searchSubmitSelector);}

    open(environment, pageName){
        let url = Environment.getBaseURL(environment,pageName)
        super.open(url); 
    }

    isHomePage(){
        return this.searchBar.isExisting();
    }

    startSearch(){
        this.search.click()
    }
}

export default new HomePage();