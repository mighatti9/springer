import Page from './page'

const resultsCountSelector = '//*[@id="kb-nav--main"]/div[1]/h1'

class SearchResultsPage extends Page {
    get resultsCount(){return browser.element(resultsCountSelector); }

    areResultsDisplayed(){
        return this.resultsCount.isExisting();
    }
}

export default new SearchResultsPage()